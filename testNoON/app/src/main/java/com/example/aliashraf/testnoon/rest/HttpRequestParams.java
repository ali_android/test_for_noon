package com.example.aliashraf.testnoon.rest;

import android.util.Pair;


public class HttpRequestParams {
    public static final String HTTP_POST = "POST";
    public static final String HTTP_GET = "GET";
    public static final String HTTP_PUT = "PUT";
    private static final int DEFAULT_REQUEST_TIMEOUT = 20000;

    private String url;
    private BodyPayLoadBase payload;
    private Pair<String, String> header;
    private String requestType;
    /**requestTimeOut - default value is 20 secs, if it has to be modified for any req,
     * use builder.setRequestTimeOut */
    private int requestTimeOut = DEFAULT_REQUEST_TIMEOUT;


    public BodyPayLoadBase getPayload() {
        return payload;
    }

    public void setPayload(BodyPayLoadBase payload) {
        this.payload = payload;
    }

    public Pair getHeader() {
        return header;
    }

    public void setHeader(Pair header) {
        this.header = header;
    }

    private HttpRequestParams(String url) {
        this.url = url;
        this.requestType = HTTP_GET;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getRequestTimeOut() {
        return requestTimeOut;
    }

    public void setRequestTimeOut(int requestTimeOut) {
        this.requestTimeOut = requestTimeOut;
    }


    public static class Builder {

        HttpRequestParams restRequest;

        public Builder(String url) {
            restRequest = new HttpRequestParams(url);
        }

        public Builder setPayload(BodyPayLoadBase payload) {
            restRequest.setPayload(payload);
            return this;
        }

        public Builder setHeader(Pair header) {
            restRequest.setHeader(header);
            return this;
        }

        public Builder setRequestType(String type) {
            restRequest.setRequestType(type);
            return this;
        }

        public Builder setRequestTimeOut(int timeOut) {
            restRequest.setRequestTimeOut(timeOut);
            return this;
        }

        public HttpRequestParams build() {
            return restRequest;
        }
    }


}
