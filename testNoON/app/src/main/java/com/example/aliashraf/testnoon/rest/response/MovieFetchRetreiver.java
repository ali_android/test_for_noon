package com.example.aliashraf.testnoon.rest.response;

import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.rest.HttpRequestParams;
import com.example.aliashraf.testnoon.rest.MovieSearchResponseListener;
import com.example.aliashraf.testnoon.rest.UrlBuilder;
import com.example.aliashraf.testnoon.rest.WorkerThreadPool;
import com.example.aliashraf.testnoon.rest.request.MovieCatalogFetchRequest;

import java.util.ArrayList;
import java.util.List;

public class MovieFetchRetreiver implements MovieSearchResponseListener {


    public interface MovieFetchRetreiverImpl {
        void onMovieListRetrieved(List<Search_> listOfMovieItems);
        void onFailToRetrieveMovie(int resultCode, String resultMessage);
    }

    private MovieFetchRetreiverImpl movieFetchRetreiverImpl;

    public void retrieve(MovieFetchRetreiverImpl movieFetchRetrieverListener, int page, String searchedMovie){
        this.movieFetchRetreiverImpl = movieFetchRetrieverListener;

        UrlBuilder urlBuilder = new UrlBuilder();

        HttpRequestParams.Builder builder = new HttpRequestParams.Builder(
                urlBuilder.getMovieCatalogFetchURL(searchedMovie , page));

        HttpRequestParams params = builder.build();
        //TODO: Building of request and its params should be task of HttpRequest type.

        MovieCatalogFetchRequest request = new MovieCatalogFetchRequest(params, this);
        WorkerThreadPool.obtain().submitTask(request);

    }

    @Override
    public void onMovieSearchResponse(List<Search_> movieList) {
        movieFetchRetreiverImpl.onMovieListRetrieved(movieList);
    }

    @Override
    public void onMovieSearchFailed(ErrorResponse errorResponse) {
        movieFetchRetreiverImpl.onFailToRetrieveMovie(errorResponse.getErrorCode(), errorResponse.getErrorMessage());
    }
}
