package com.example.aliashraf.testnoon.rest;

/**
 * This class builds the url for the different rest request based on the information given to it.
 */
public class UrlBuilder {
    public String url = "http://www.omdbapi.com/?apikey=b1907251";
    //http://www.omdbapi.com/?apikey=b1907251&s=English&page=30
    //http://www.omdbapi.com/?apikey=b1907251&i=tt0108778

    public UrlBuilder(String url) {
        this.url = url;
    }

    public UrlBuilder() {

    }

    public String getMovieCatalogFetchURL(String searchedMovie, int page){
        StringBuilder builder = new StringBuilder(url);
        builder.append("&s="+searchedMovie);
        builder.append("&page="+page);
        return builder.toString();
    }

    public String getMovieDetailURL(String id){
        StringBuilder builder = new StringBuilder(url);
        builder.append("&i="+id);
        return builder.toString();
    }



    //Intentionally made it static so as to use directly with Glide.
    public static String getFullImageURL(String imageUrl){
        StringBuilder builder = new StringBuilder(imageUrl);
        return builder.toString();
    }
}
