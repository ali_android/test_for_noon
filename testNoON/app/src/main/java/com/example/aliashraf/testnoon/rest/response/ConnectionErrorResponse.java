package com.example.aliashraf.testnoon.rest.response;
/**Represents Connection Error, during a REST request with server.*/
public class ConnectionErrorResponse extends ErrorResponse {
    private int mErrorCode;
    private String mErrorMessage;


    public ConnectionErrorResponse(int errorCode, String errorMessage){
        this.mErrorCode = errorCode;
        this.mErrorMessage = errorMessage;
    }

    @Override
    public int getErrorCode() {
        return mErrorCode;
    }

    @Override
    public String getErrorMessage() {
        return mErrorMessage;
    }
}
