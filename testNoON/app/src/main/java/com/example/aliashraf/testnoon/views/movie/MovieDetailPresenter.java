package com.example.aliashraf.testnoon.views.movie;

import android.os.Handler;
import android.os.Looper;

import com.example.aliashraf.testnoon.rest.response.MovieDetailRetreiver;
import com.example.aliashraf.testnoon.views.BasePresenter;
import com.example.aliashraf.testnoon.views.movie.model.MovieDetail;

class MovieDetailPresenter extends BasePresenter implements MovieDetailRetreiver.MovieDetailRetrieverImpl {


    private String movieID;
    private MovieCatalogScreenContract.MovieDetailScreenOperations contract;
    MovieDetailPresenter(String movieID, MovieCatalogScreenContract.MovieDetailScreenOperations contract){
        this.movieID = movieID;
        this.contract = contract;
    }

    void fetchMovieDetails(){
        MovieDetailRetreiver retreiver = new MovieDetailRetreiver();
        retreiver.retrieve(this, movieID);
    }

    /**
     * Clean up all view related resources here.
     */
    @Override
    protected void cleanUpViewResources() {
        contract = null;
    }

    @Override
    public void onMovieDetailRetrieved(final MovieDetail movieDetail) {
        Handler mainHandler = new Handler(Looper.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if(contract!=null) {
                    contract.onMovieDetailsRetrieved(movieDetail);
                }else{
                    return;
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public void onFailToRetrieveMovieDetails(final int resultCode, final String resultMessage) {
        Handler mainHandler = new Handler(Looper.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                if(contract!=null) {
                    contract.onFailToRetrieveMovieDetails(resultCode, resultMessage);
                }else{
                    return;
                }
            }
        };
        mainHandler.post(myRunnable);
    }
}
