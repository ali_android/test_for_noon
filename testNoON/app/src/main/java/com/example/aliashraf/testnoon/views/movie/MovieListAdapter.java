package com.example.aliashraf.testnoon.views.movie;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aliashraf.testnoon.R;
import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.rest.UrlBuilder;
import com.example.aliashraf.testnoon.views.CircularImageView;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements Filterable {


    private List<Search_> movieList;
    private MovieSelectedListener listener;

    private Context context;
    private static final int ITEM = 0;
    private static final int LOADING = 1;
    private boolean isLoadingAdded = false;

    MovieListAdapter(List<Search_> movieList, MovieSelectedListener listener, Context context) {

        this.movieList = movieList;
        this.listener = listener;
        this.context = context;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        RecyclerView.ViewHolder viewHolder = null;

        switch (viewType) {
            case ITEM:
                viewHolder = getViewHolder(parent, inflater);
                break;
            case LOADING:
                View v = inflater.inflate(R.layout.item_progress, parent, false);
                viewHolder = new ProgressViewHolder(v);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        bindData(holder, position);
    }

    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View view = inflater.inflate(R.layout.movie_list_row, parent, false);
        viewHolder = new MovieViewHolder(view);
        return viewHolder;
    }


    private void bindData(RecyclerView.ViewHolder holder, int position) {

        final Search_ movie = movieList.get(position);
        switch (getItemViewType(position)) {
            case ITEM:
                MovieViewHolder holder1 = (MovieViewHolder) holder;
                holder1.movieName.setText(movie.getTitle());
                holder1.movieYear.setText(movie.getYear());
                Glide.with(context)
                        .load(UrlBuilder.getFullImageURL(movie.getPoster()))
                        .into(holder1.image);
                holder1.itemLayout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        listener.onMovieSelected(movie);
                    }
                });
                break;
            case LOADING:
                Log.d("", "Loading view will be added.");
                break;
        }


    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    @Override
    public Filter getFilter() {
        return null;
    }

    @Override
    public int getItemViewType(int position) {
        if ((position == movieList.size() - 1) && (isLoadingAdded)) {
            return LOADING;
        } else {
            return ITEM;
        }
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;
    }

    protected class MovieViewHolder extends RecyclerView.ViewHolder {

        RelativeLayout itemLayout;
        CircularImageView image;
        TextView movieName;
        TextView movieYear;
        CheckBox isBookMarked;

        public MovieViewHolder(View itemView) {
            super(itemView);
            itemLayout              =  itemView.findViewById(R.id.movieLayout);
            image                   =  itemView.findViewById(R.id.image);
            movieName               =  itemView.findViewById(R.id.movieName);
            movieYear               = itemView.findViewById(R.id.movie_year);
            isBookMarked            = itemView.findViewById(R.id.favourite);
        }
    }

    protected class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View itemView) {
            super(itemView);
        }
    }
}
