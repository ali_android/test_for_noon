package com.example.aliashraf.testnoon.views;

public abstract class BasePresenter {

    /**
     * Clean up all view related resources here.
     * */
    protected abstract void cleanUpViewResources();

    /**
     * Override this method and do unregister of any event listeners.
     * Make sure to call super(), every time so that ViewsPresenterBase can unregister
     * its listeners.
     *
     * @CallSuper
     * */
    protected void unRegisterListeners(){

    }
}
