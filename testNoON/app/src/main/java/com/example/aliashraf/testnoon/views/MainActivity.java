package com.example.aliashraf.testnoon.views;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.example.aliashraf.testnoon.R;
import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.views.movie.MovieCatalogScreen;
import com.example.aliashraf.testnoon.views.movie.MovieDetailScreen;
import com.example.aliashraf.testnoon.views.movie.MovieSelectedListener;

public class MainActivity extends AppCompatActivity implements MovieSelectedListener{


    private MovieCatalogScreen mCatalogScreen;
    MovieDetailScreen movieDetailScreen;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //http://www.omdbapi.com/?i=tt3896198&apikey=b1907251
        //Search all movies by word English, with pagination, each page has 10 movies
        //http://www.omdbapi.com/?apikey=b1907251&s=English&page=30
        //http://www.omdbapi.com/?apikey=b1907251&s=friends
        //failed to find a movie:
        /*{"Response":"False","Error":"Movie not found!"}*/

        //Search movie by imdb id: IMDB id will be received by search api.
        //http://www.omdbapi.com/?apikey=b1907251&i=tt0108778

        //Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);

        if(savedInstanceState == null){
            //fresh launch.
            mCatalogScreen = new MovieCatalogScreen();
            attachFragmentToContainer(mCatalogScreen);
        }

    }

    void attachFragmentToContainer(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        Fragment f = getFragmentManager().findFragmentByTag(fragment.getClass().toString());
        if(f != null){
            if(! f.isAdded()) {
                fragmentTransaction.add(R.id.fragment_container, f, f.getClass().toString());
                f.setArguments(getIntent().getExtras());
            }
        }
        else{
            fragmentTransaction.add(R.id.fragment_container, fragment, fragment.getClass().toString());
            fragment.setArguments(getIntent().getExtras());
        }
        fragmentTransaction.commit();
    }

    private void replaceFragment(Fragment fragment, Bundle bundle, boolean addTobackStack) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment f = getFragmentManager().findFragmentByTag(fragment.getClass().toString());

        if(f != null){
            if(bundle != null)
                f.setArguments(bundle);

            transaction.replace(R.id.fragment_container, f);
        }
        else{
            if(bundle != null)
                fragment.setArguments(bundle);

            transaction.replace(R.id.fragment_container, fragment);
        }

        if(addTobackStack){
            transaction.addToBackStack("com.example.aliashraf.testnoon");
        }
        // Commit the transaction
        transaction.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_catalog_search, menu);
        return true;
    }

    /**
     * On a movie selected, from list of movies.
     *
     * @param movie
     */
    @Override
    public void onMovieSelected(Search_ movie) {
        //Launch detail screen and display the movie details.
        movieDetailScreen = new MovieDetailScreen();
        Bundle bundle = new Bundle();
        bundle.putString("MovieActivity.Movie.poster", movie.getPoster());
        bundle.putString("MovieActivity.Movie.imdbID",movie.getImdbID());
        replaceFragment(movieDetailScreen, bundle, true);
    }
}
