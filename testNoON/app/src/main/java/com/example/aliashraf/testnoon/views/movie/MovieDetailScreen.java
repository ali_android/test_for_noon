package com.example.aliashraf.testnoon.views.movie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.aliashraf.testnoon.R;
import com.example.aliashraf.testnoon.rest.UrlBuilder;
import com.example.aliashraf.testnoon.views.BaseFragment;
import com.example.aliashraf.testnoon.views.movie.model.MovieDetail;

public class MovieDetailScreen extends BaseFragment implements MovieCatalogScreenContract.MovieDetailScreenOperations {

    private String imdbID;
    private String moviePoster;
    private ImageView image;
    //Name, Year, Genre, Director, image, IMDB rating, Actors.
    private TextView name, director, year, genre, rating, actors;
    private MovieDetailPresenter presenter;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            /*pass this value to presenter, to show all details.*/
            imdbID = bundle.getString("MovieActivity.Movie.imdbID");
            moviePoster = bundle.getString("MovieActivity.Movie.poster");
        }
        presenter = new MovieDetailPresenter(imdbID, this);
        presenter.fetchMovieDetails();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movie_detail, container, false);
        image = (ImageView)view.findViewById(R.id.image) ;
        name = (TextView)view.findViewById(R.id.movieTitle);
        director = (TextView)view.findViewById(R.id.movie_director_name);
        year = (TextView)view.findViewById(R.id.movie_year);

        genre = (TextView)view.findViewById(R.id.movie_genre);
        rating = (TextView)view.findViewById(R.id.movie_rating);
        actors = (TextView)view.findViewById(R.id.movie_actors);

        /*productItemTitle.setText(productItem.getProductItemTitle());
        productDescription.setText("Description : "+productItem.getProductItemDescription());
        productItemPrice.setText("Price : "+productItem.getProductItemPrice() +"$");*/

        Glide.with(getActivity())
                .load(UrlBuilder.getFullImageURL(moviePoster))
                .into(image);
        return view;
    }

    @Override
    public void onMovieDetailsRetrieved(MovieDetail movieDetail) {
        name.setText("Title : "+movieDetail.getTitle());
        director.setText("Director : "+movieDetail.getDirector());
        year.setText("Year : "+movieDetail.getYear());
        genre.setText("Genre : "+movieDetail.getGenre());
        rating.setText("Rating : "+movieDetail.getImdbRating());
        actors.setText("Actors : "+movieDetail.getActors());
    }

    @Override
    public void onFailToRetrieveMovieDetails(int resultCode, String resultMessage) {

    }
}
