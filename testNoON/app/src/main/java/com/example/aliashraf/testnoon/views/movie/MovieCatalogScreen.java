package com.example.aliashraf.testnoon.views.movie;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.aliashraf.testnoon.R;
import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.views.BaseFragment;

import java.util.ArrayList;
import java.util.List;

public class MovieCatalogScreen extends BaseFragment implements MovieCatalogScreenContract.MovieCatalogScreenOperations {


    private MovieCatalogScreenViewPresenter mPresenter;
    private RecyclerView movieListView;

    private List<Search_> mMovieList;
    private MovieListAdapter movieListAdapter;
    private LinearLayoutManager movieLayoutManager;
    private ProgressBar progressBar;

    private MovieSelectedListener mMovieSelectedListener;

    private EditText search_edit_text;
    private Button search_button;

    private String searchedMovie;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(savedInstanceState == null){
            mPresenter = new MovieCatalogScreenViewPresenter();
            mMovieList = new ArrayList<Search_>();
        }
        else{
            mPresenter = savedInstanceState.getParcelable("com.example.aliashraf.testnoon.views.movie.PRESENTER");
            mMovieList = savedInstanceState.getParcelableArrayList("com.example.aliashraf.testnoon.views.movie.MOVIELIST");
        }

        //mPresenter.fetchMovieItems("Friends");
        mMovieSelectedListener = (MovieSelectedListener) getActivity();
        mPresenter.registerForCatalogScreenOperations(this);
        movieListAdapter = new MovieListAdapter(mMovieList, mMovieSelectedListener, getActivity().getApplicationContext());

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.cleanUpViewResources();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movie_search, container, false);
        movieListView = (RecyclerView)view.findViewById(R.id.movieRecyclerview);
        progressBar = (ProgressBar)view. findViewById(R.id.main_progress);
        search_edit_text = view.findViewById(R.id.search_edit_text);
        search_button = view.findViewById(R.id.search_button);

        movieLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        movieListView.setLayoutManager(movieLayoutManager);
        movieListView.setItemAnimator(new DefaultItemAnimator());
        movieListView.getRecycledViewPool().setMaxRecycledViews(0,0);
        movieListView.setAdapter(movieListAdapter);
        movieListView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            /**
             * Callback method to be invoked when RecyclerView's scroll state changes.
             *
             */
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                /*if (!recyclerView.canScrollVertically(1)) {
                    //Toast.makeText(getActivity(),"End is reached.", Toast.LENGTH_LONG).show();
                }*/
            }
            /**
             * Callback method to be invoked when the RecyclerView has been scrolled. This will be
             * called after the scroll has completed.
             * <p>
             * This callback will also be called if visible item range changes after a layout
             * calculation. In that case, dx and dy will be 0.
             */
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = movieLayoutManager.getChildCount();
                int totalItemCount = movieLayoutManager.getItemCount();
                int firstVisibleItemPosition = movieLayoutManager.findFirstVisibleItemPosition();
                if (!mPresenter.isLoading()) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        movieListAdapter.addLoadingFooter();
                        mPresenter.fetchMovieItems(search_edit_text.getText().toString());
                    }
                }
            }
        });

        search_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMovieList.clear();
                movieListAdapter.notifyDataSetChanged();
                mPresenter.fetchMovieItems(search_edit_text.getText().toString());
            }
        });

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList("com.example.aliashraf.testnoon.views.movie.MOVIELIST", (ArrayList<? extends Parcelable>) mMovieList);
        outState.putParcelable("com.example.aliashraf.testnoon.views.movie.PRESENTER", mPresenter);
    }

    @Override
    public void onMovieCatalogListRetrieved(List<Search_> listOfMovieItems) {
        movieListAdapter.removeLoadingFooter();
        progressBar.setVisibility(View.GONE);
        /*This logic can be optimised in terms of space complexity, keep on addING into list
        will take up too much space.*/
        if(listOfMovieItems != null) {
            mMovieList.addAll(listOfMovieItems);
            movieListAdapter.notifyDataSetChanged();
        }else{
            onFailToRetrieveMovies(3,"Movie not found");
        }
    }

    @Override
    public void onFailToRetrieveMovies(int resultCode, String resultMessage) {
        progressBar.setVisibility(View.GONE);
        movieListAdapter.removeLoadingFooter();
        Toast.makeText(getActivity(),resultMessage, Toast.LENGTH_LONG).show();
    }
}
