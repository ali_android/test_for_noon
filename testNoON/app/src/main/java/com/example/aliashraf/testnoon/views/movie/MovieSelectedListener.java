package com.example.aliashraf.testnoon.views.movie;


import com.example.aliashraf.testnoon.model.Search_;

public interface MovieSelectedListener {

    /**On a movie selected, from list of movies.*/
    void onMovieSelected(Search_ movie);
}
