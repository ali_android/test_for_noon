package com.example.aliashraf.testnoon.rest.response;

import com.example.aliashraf.testnoon.rest.HttpRequestParams;
import com.example.aliashraf.testnoon.rest.MovieDetailResponseListener;
import com.example.aliashraf.testnoon.rest.UrlBuilder;
import com.example.aliashraf.testnoon.rest.WorkerThreadPool;
import com.example.aliashraf.testnoon.rest.request.MovieDetailFetchRequest;
import com.example.aliashraf.testnoon.views.movie.model.MovieDetail;

public class MovieDetailRetreiver implements MovieDetailResponseListener {


    public interface MovieDetailRetrieverImpl {
        void onMovieDetailRetrieved(MovieDetail movieDetail);
        void onFailToRetrieveMovieDetails(int resultCode, String resultMessage);
    }

    private MovieDetailRetrieverImpl movieDetailRetreiverImpl;

    public void retrieve(MovieDetailRetrieverImpl movieDetailRetreiverImpl, String id){
        this.movieDetailRetreiverImpl = movieDetailRetreiverImpl;

        UrlBuilder urlBuilder = new UrlBuilder();

        HttpRequestParams.Builder builder = new HttpRequestParams.Builder(
                urlBuilder.getMovieDetailURL(id));

        HttpRequestParams params = builder.build();
        //TODO: Building of request and its params should be task of HttpRequest type.

        MovieDetailFetchRequest request = new MovieDetailFetchRequest(params, this);
        WorkerThreadPool.obtain().submitTask(request);

    }

    @Override
    public void onMovieDetailResponse(MovieDetail movieDetail) {

        movieDetailRetreiverImpl.onMovieDetailRetrieved(movieDetail);
    }

    @Override
    public void onMovieDetailRequestFailed(ErrorResponse errorResponse) {

        movieDetailRetreiverImpl.onFailToRetrieveMovieDetails(errorResponse.getErrorCode(),
                errorResponse.getErrorMessage());
    }
}
