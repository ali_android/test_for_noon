package com.example.aliashraf.testnoon.rest;

import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.rest.response.ErrorResponse;

import java.util.List;

/**Callback listener for Movie Catalog search.*/
public interface MovieSearchResponseListener {

    void onMovieSearchResponse(List<Search_> movieList);
    void onMovieSearchFailed(ErrorResponse errorResponse);
}
