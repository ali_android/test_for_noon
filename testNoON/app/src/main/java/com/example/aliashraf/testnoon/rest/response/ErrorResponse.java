package com.example.aliashraf.testnoon.rest.response;
/**Represents REST Error response from server*/
public abstract class ErrorResponse {
    public abstract int getErrorCode();
    public abstract String getErrorMessage();
}
