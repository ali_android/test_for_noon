package com.example.aliashraf.testnoon.rest;

import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.rest.response.ErrorResponse;
import com.example.aliashraf.testnoon.views.movie.model.MovieDetail;

import java.util.List;

public interface MovieDetailResponseListener {
    void onMovieDetailResponse(MovieDetail movieDetail);
    void onMovieDetailRequestFailed(ErrorResponse errorResponse);
}
