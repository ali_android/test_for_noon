package com.example.aliashraf.testnoon.rest;

import android.util.Log;

import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**Singleton that handles a pool of threads using ThreadPoolExecutor.*/
public class WorkerThreadPool {

    private static WorkerThreadPool singleInstance;
    ThreadPoolExecutor executor;
    private static int NUMBER_OF_CORES = Runtime.getRuntime().availableProcessors();
    private static int KEEP_ALIVE_TIME_OUT = 1;
    private LinkedBlockingQueue taskQueue;
    private static final String TAG = WorkerThreadPool.class.getName();

    private WorkerThreadPool() {
        taskQueue = new LinkedBlockingQueue();
        executor = new ThreadPoolExecutor(NUMBER_OF_CORES, NUMBER_OF_CORES, KEEP_ALIVE_TIME_OUT, TimeUnit.SECONDS, taskQueue);
    }

    public static WorkerThreadPool obtain() {
        if (singleInstance == null) {
            synchronized (WorkerThreadPool.class) {
                singleInstance = new WorkerThreadPool();
            }
        }
        return singleInstance;
    }

    public void submitTask(Runnable runnable) {
        Log.d(TAG,"submitTask() ::: called and Runnable task will be submitted now for "+runnable.getClass().toString());
        try {
            executor.execute(runnable);
        }catch(RejectedExecutionException e){
            Log.d(TAG,"submitTask() ::: RejectedExecutionException caught : "+e.toString());
            if(executor.isTerminated()){
                Log.d(TAG,"submitTask() ::: Executor was terminated : reinitialising Executor and sumbiting the task again for : "+runnable.getClass().toString());
                executor = new ThreadPoolExecutor(NUMBER_OF_CORES, NUMBER_OF_CORES, KEEP_ALIVE_TIME_OUT, TimeUnit.SECONDS, taskQueue);
                submitTask(runnable);
            }
        }
    }

    void shutdownWorkerThreadPool() {
        Log.d(TAG,"shutdown() ::: called, executor will shutdown gracefully after previously submitted tasks are executed.");
        executor.shutdown();
    }

    List<Runnable> abort() {
        Log.d(TAG,"abort() ::: called, executor will shutdown now.");
        List<Runnable> listOfUnexecutedRunnables = executor.shutdownNow();
        return listOfUnexecutedRunnables;
    }
}
