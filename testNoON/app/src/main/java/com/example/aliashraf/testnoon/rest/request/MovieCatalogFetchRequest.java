package com.example.aliashraf.testnoon.rest.request;

import com.example.aliashraf.testnoon.rest.response.Search;
import com.example.aliashraf.testnoon.rest.HttpRequest;
import com.example.aliashraf.testnoon.rest.HttpRequestParams;
import com.example.aliashraf.testnoon.rest.MovieSearchResponseListener;
import com.example.aliashraf.testnoon.rest.response.ConnectionErrorResponse;
import com.example.aliashraf.testnoon.rest.response.ErrorResponse;
import com.fasterxml.jackson.databind.ObjectMapper;


/**A runnable, to fetch list of Movies */
public class MovieCatalogFetchRequest extends HttpRequest {

    private MovieSearchResponseListener listener;

    public MovieCatalogFetchRequest(HttpRequestParams params, MovieSearchResponseListener listener) {
        super(params);
        this.listener = listener;
    }



    @Override
    protected void onResult(int resultCode, String jsonResponse) throws java.io.IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        Search response = objectMapper.readValue(jsonResponse, Search.class);
        listener.onMovieSearchResponse(response.getSearch());
    }

    /**
     * Called, stating this is the right time to making member variables ready for GC
     */
    @Override
    protected void releaseMemory() {
        listener = null;
    }

    /**
     * Called in case of any exception, server error.
     *
     * @param resultCode      result code returned by server if any, exception error codes.
     *                        Exception error codes have been decided by us and have range from 1 - 99,
     *                        because HTTP Status Codes starts from 100, thus using exception error codes
     *                        from 1 to 99, so that there are no clash.
     * @param jsonErrorString Json String returned from Server, if any, of exception string.
     */
    @Override
    protected void onError(int resultCode, String jsonErrorString) {
        ObjectMapper objectMapper = new ObjectMapper();
        ErrorResponse errorResponse = null;
        try {
            errorResponse = objectMapper.readValue(
                    jsonErrorString, ErrorResponse.class);

        } catch (java.io.IOException e) {
            e.printStackTrace();
            errorResponse = new ConnectionErrorResponse(resultCode, jsonErrorString);
        }
        listener.onMovieSearchFailed(errorResponse);
    }
}
