package com.example.aliashraf.testnoon.views.movie.model;

import android.os.Parcel;
import android.os.Parcelable;

public class MovieDetail implements Parcelable {

    private String title;
    private String year;
    private String genre;
    private String director;
    private String actors;
    private String imdbRating;
    private String imdbID;

    public MovieDetail(String title,
             String year,
             String genre,
             String director,
             String actors,
             String imdbRating,
             String imdbID){
        this.actors = actors;
        this.title = title;
        this.year = year;
        this.genre = genre;
        this.director = director;
        this.imdbRating = imdbRating;
        this.imdbID = imdbID;
    }

    protected MovieDetail(Parcel in) {
        title = in.readString();
        year = in.readString();
        genre = in.readString();
        director = in.readString();
        actors = in.readString();
        imdbRating = in.readString();
        imdbID = in.readString();
    }

    public static final Creator<MovieDetail> CREATOR = new Creator<MovieDetail>() {
        @Override
        public MovieDetail createFromParcel(Parcel in) {
            return new MovieDetail(in);
        }

        @Override
        public MovieDetail[] newArray(int size) {
            return new MovieDetail[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public String getYear() {
        return year;
    }

    public String getGenre() {
        return genre;
    }

    public String getDirector() {
        return director;
    }

    public String getActors() {
        return actors;
    }

    public String getImdbRating() {
        return imdbRating;
    }

    public String getImdbID() {
        return imdbID;
    }

    @Override
    public int describeContents() {
        return 0;
    }


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(year);
        dest.writeString(genre);
        dest.writeString(director);
        dest.writeString(actors);
        dest.writeString(imdbRating);
        dest.writeString(imdbID);
    }
}
