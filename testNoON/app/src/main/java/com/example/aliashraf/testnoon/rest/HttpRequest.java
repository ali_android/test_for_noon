package com.example.aliashraf.testnoon.rest;

import android.support.annotation.IntDef;
import android.util.Log;
import android.util.Pair;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Base class to perform http operations to communicate with server
 */
public abstract class HttpRequest implements Runnable {
    protected HttpRequestParams params;

    //Exception Error Codes
    protected static final int MalformedURLException = 1;
    protected static final int ProtocolException = 2;
    protected static final int ConnectException = 3;
    protected static final int SocketTimeoutException = 4;
    protected static final int IOException = 5;
    protected static final int Exception = 6;
    protected static final int NullPointerException = 7;

    @IntDef({ProtocolException, MalformedURLException, ConnectException, SocketTimeoutException, IOException, Exception, NullPointerException})

    @Retention(RetentionPolicy.SOURCE)
    public @interface ExceptionErrorCodes {
    }

    //TODO :add support for https
    public HttpRequest(HttpRequestParams params) {
        this.params = params;
        TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
            public X509Certificate[] getAcceptedIssuers() {
                return null;
            }
            public void checkClientTrusted(X509Certificate[] certs, String authType) {
            }
            public void checkServerTrusted(X509Certificate[] certs, String authType) {
            }
        } };
        // Install the all-trusting trust manager
        SSLContext sc = null;
        try {
            sc = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try {
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        // Create all-trusting host name verifier
        HostnameVerifier allHostsValid = new HostnameVerifier() {
            public boolean verify(String hostname, SSLSession session) {
                return true;
            }
        };

        // Install the all-trusting host verifier
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
    }

    public HttpRequest() {
    }


    protected void performHttpCall() {
        InputStream is = null;
        URL url = null;
        try {
            url = new URL(params.getUrl());
        } catch (java.net.MalformedURLException e) {
            e.printStackTrace();
            Log.d("performHttpCall ", "MalformedURLException Caught  " + e.toString());
            //onError(e, response);
            return;
        }

        HttpURLConnection conn = null;
        int responseCode = 0;
        String responseStatusMessage = null;
        try {
            conn = (HttpURLConnection) url.openConnection();

            //print_https_cert(conn);
            conn.setReadTimeout(10000 /* milliseconds */);
            //conn.setConnectTimeout(params.getRequestTimeOut());
            conn.setConnectTimeout(10000);
            conn.setRequestMethod(params.getRequestType());
            conn.setDoInput(true);

            BodyPayLoadBase payload = params.getPayload();
            if (payload != null) {
                OutputStreamWriter writer = new OutputStreamWriter(conn.getOutputStream(), "UTF-8");
                ObjectMapper mapper = new ObjectMapper();


                String payloadString = mapper.writeValueAsString(payload);
                Log.d("performHttpCall" , "::: payloadString is: " + payloadString);
                writer.write(payloadString);
                writer.close();
            }
            Pair<String, String> pair = params.getHeader();
            if (pair != null) {
                conn.setRequestProperty(pair.first, pair.second);
            }

            // Starts the query
            conn.connect();
            responseCode = conn.getResponseCode();
            responseStatusMessage = conn.getResponseMessage();
            Log.d("performHttpCall" , "::: The responseStatusMessage is: " + responseStatusMessage);
            Log.d("performHttpCall" , "::: The response code is: " + responseCode);
            is = conn.getInputStream();
            Log.d("performHttpCall" , " ::: Input stream received : ");
            String stringFromInputStream = getStringFromInputStream(is);
            Log.d("performHttpCall" , " ::: string From InputStream is - " + stringFromInputStream);
            onResult(responseCode, stringFromInputStream);
        } catch (java.net.ProtocolException e) {
            Log.d("performHttpCall" , " ::: ProtocolException Caught  " + e.toString());
            Log.d("performHttpCall" , " ::: ProtocolException Caught message " + e.getMessage());
            onError(ProtocolException, e.toString());
            e.printStackTrace();
        } catch (java.net.ConnectException connectException) {
            Log.d("performHttpCall" , " ::: ConnectException Caught  " + connectException.toString());
            Log.d("performHttpCall" , "::: ConnectException Caught message " + connectException.getMessage());
            connectException.printStackTrace();
            onError(ConnectException, connectException.toString());
        }
        catch (java.net.SocketTimeoutException e) {
            Log.d("performHttpCall" , " ::: SocketTimeoutException Caught  " + e.toString());
            Log.d("performHttpCall" , " ::: SocketTimeoutException Caught message " + e.getMessage());
            e.printStackTrace();
            onError(SocketTimeoutException, e.toString());
        } catch (java.io.IOException e) {
            Log.d("performHttpCall" , " ::: IOException Caught  " + e.toString());
            Log.d("performHttpCall" , " ::: IOException Caught message " + e.getMessage());
            if (conn != null) {
                is = conn.getErrorStream();
                if (is != null) {
                    Log.d("performHttpCall" , ":::IOException Caught,  error stream received ");
                    String stringFromErrorStream = getStringFromInputStream(is);
                    Log.d("performHttpCall" , " :::Error Stream is :::  " + stringFromErrorStream);
                    onError(responseCode, stringFromErrorStream);
                } else {
                    Log.d("performHttpCall" , " :::Error Stream is null");
                    if(responseStatusMessage != null){
                        onError(NullPointerException, responseStatusMessage);
                    }
                    else{
                        Log.d("performHttpCall" , " ::: responseStatusMessage is also null. Contact server admin and report the issue.");
                        onError(NullPointerException, "Response Status Message is null");
                    }

                }
            } else {
                //conn is null.
                onError(Exception, e.toString());
            }
            e.printStackTrace();
        }catch(java.lang.Exception e) {
            Log.d("performHttpCall" , "Exception Caught  " + e.toString());
            Log.d("performHttpCall" , "Exception Caught Message " + e.getMessage());
            onError(Exception, e.toString());
            e.printStackTrace();
        } finally {
            Log.d("performHttpCall" , "Finally Block called. ");
            //releaseMemory();
        }
    }

    protected abstract void onResult(int resultCode, String jsonResponse) throws java.io.IOException;
    //TODO: Add an abstract method to release all resources.
    /**
     * Called, stating this is the right time to making member variables ready for GC*/
    protected abstract void releaseMemory();

    /**
     * Called in case of any exception, server error.
     *
     * @param resultCode      result code returned by server if any, exception error codes.
     *                        Exception error codes have been decided by us and have range from 1 - 99,
     *                        because HTTP Status Codes starts from 100, thus using exception error codes
     *                        from 1 to 99, so that there are no clash.
     * @param jsonErrorString Json String returned from Server, if any, of exception string.
     */
    protected abstract void onError(int resultCode, String jsonErrorString);

    /**
     * @param is Inputstream from http connection
     * @return convert stream to string
     */
    private String getStringFromInputStream(InputStream is) {
        Log.d("getStringFromInputStrm ", "::: Called, Thread is = " + Thread.currentThread().getName());
        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();
        String line;
        try {
            Log.d("getStringFromInputStrm ", "::: inside try block ");
            br = new BufferedReader(new InputStreamReader(is));
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
        } catch (java.io.IOException e) {
            Log.d("getStringFromInputStrm ", " ::: IOException caught ");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (java.io.IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return sb.toString();
    }

    @Override
    public void run() {
        performHttpCall();
        /*try {
            while (!Thread.interrupted()) {
                performHttpCall();
            }
        }
        catch (Exception e){
            WorkerThreadPool.obtain().abort();
        }*/

    }
}
