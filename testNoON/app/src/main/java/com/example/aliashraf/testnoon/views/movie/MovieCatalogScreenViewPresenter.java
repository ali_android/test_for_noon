package com.example.aliashraf.testnoon.views.movie;

import android.os.Handler;
import android.os.Looper;
import android.os.Parcel;
import android.os.Parcelable;

import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.rest.response.MovieFetchRetreiver;
import com.example.aliashraf.testnoon.views.BasePresenter;

import java.util.List;
/**
 * Deals with logic of getting the content to be shown on MovieListScreen.
 */
class MovieCatalogScreenViewPresenter extends BasePresenter implements
        MovieFetchRetreiver.MovieFetchRetreiverImpl, Parcelable {

    //int pageSize = 20;
    int pageNumber = 0;
    private boolean isPageLoading;
    private String searchedMovie;
    private MovieCatalogScreenContract.MovieCatalogScreenOperations movieScreenContract;

    MovieCatalogScreenViewPresenter(){
    }

    /*void onMovieListScrolledCompletely() {
        //pageSize = pageSize+20;
        //if(pageSize == 100){
            //pageNumber++;
       //     pageSize = 0;
       // }
        //fetchMovieItems(pageNumber, pageSize);
    }*/

    public void fetchMovieItems(String searchedMovie) {

        if(this.searchedMovie == null){
            this.searchedMovie = searchedMovie;//new search
        }else{
            if(!this.searchedMovie.equalsIgnoreCase(searchedMovie)){//old fragment search with new movie name.
                pageNumber = 0;
            }
        }
        pageNumber++;
        isPageLoading = true;
        MovieFetchRetreiver retriever = new MovieFetchRetreiver();
        retriever.retrieve(this, pageNumber, searchedMovie);
    }

    protected MovieCatalogScreenViewPresenter(Parcel in) {
        //pageSize = in.readInt();
        pageNumber = in.readInt();
        isPageLoading = in.readByte() != 0;
    }

    public static final Creator<MovieCatalogScreenViewPresenter> CREATOR = new Creator<MovieCatalogScreenViewPresenter>() {
        @Override
        public MovieCatalogScreenViewPresenter createFromParcel(Parcel in) {
            return new MovieCatalogScreenViewPresenter(in);
        }

        @Override
        public MovieCatalogScreenViewPresenter[] newArray(int size) {
            return new MovieCatalogScreenViewPresenter[size];
        }
    };

    public boolean isLoading() {
        return isPageLoading;
    }

    public void registerForCatalogScreenOperations(MovieCatalogScreenContract.MovieCatalogScreenOperations movieScreenContract){
        this.movieScreenContract = movieScreenContract;
    }

    @Override
    protected void unRegisterListeners() {
        super.unRegisterListeners();
    }

    /**
     * Clean up all view related resources here.
     */
    @Override
    protected void cleanUpViewResources() {
        movieScreenContract = null;
    }


    @Override
    public void onMovieListRetrieved(final List<Search_> listOfMovieItems) {
        Handler mainHandler = new Handler(Looper.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isPageLoading = false;
                if(movieScreenContract!=null) {
                    movieScreenContract.onMovieCatalogListRetrieved(listOfMovieItems);
                }else{
                    return;
                }
            }
        };
        mainHandler.post(myRunnable);

    }

    @Override
    public void onFailToRetrieveMovie(final int resultCode, final String resultMessage) {
        Handler mainHandler = new Handler(Looper.getMainLooper());

        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                isPageLoading = false;
                if(movieScreenContract!=null) {
                    movieScreenContract.onFailToRetrieveMovies(resultCode, resultMessage);
                }else{
                    return;
                }
            }
        };
        mainHandler.post(myRunnable);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
       // dest.writeInt(pageSize);
        dest.writeInt(pageNumber);
        dest.writeByte((byte) (isPageLoading ? 1 : 0));
    }
}
