package com.example.aliashraf.testnoon.views.movie;


import com.example.aliashraf.testnoon.model.Search_;
import com.example.aliashraf.testnoon.views.movie.model.MovieDetail;

import java.util.List;

public class MovieCatalogScreenContract {

    public interface MovieCatalogScreenOperations{
        void onMovieCatalogListRetrieved(List<Search_> listOfMovieItems);
        void onFailToRetrieveMovies(int resultCode, String resultMessage);
    }

    public interface MovieDetailScreenOperations{
        void onMovieDetailsRetrieved(MovieDetail movieDetail);
        void onFailToRetrieveMovieDetails(int resultCode, String resultMessage);
    }
}
